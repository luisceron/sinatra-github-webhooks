class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.references :issue, foreign_key: true, index: true
      t.string :action
    end
  end
end
