class Event < ActiveRecord::Base
  belongs_to :issue

  validates :issue, presence: true
  validates :action, presence: true
end
