class Issue < ActiveRecord::Base
  has_many :events

  validates :number, presence: true
  validates :title, presence: true
end
