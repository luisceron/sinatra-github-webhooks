require 'sinatra'
require 'sinatra/activerecord'
require 'pry'
require './models/event'
require './models/issue'
require 'json'

get '/issues/:number/events' do
  issue = Issue.find_by(number: params[:number])
  if issue
    issue.events.to_json
  else
    { message: 'Issue Not found' }.to_json
  end
end

post '/events' do
  body = JSON.parse(request.body.read)

  issue = Issue.find_by(number: body['issue']['number'])
  if issue
    issue.events.create!(action: body['action'])
    { message: 'Issue Updated' }.to_json
  else
    issue = Issue.create!(number: body['issue']['number'], title: body['issue']['title'])
    issue.events.create!(action: body['action'])
    { message: 'New Issue Created' }.to_json
  end
end

# binding.pry
